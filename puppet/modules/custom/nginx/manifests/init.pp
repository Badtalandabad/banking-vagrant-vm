class nginx {
  package { 'nginx':
    ensure => 'present',
    require => Exec['apt-get update'],
  }

  # Hosts
  file { 'nginx-host-default':
    path    => "/etc/nginx/sites-available/default",
    ensure  => file,
    require => Package['nginx'],
    notify  => Service['nginx'],
    content => template('nginx/default.erb'),
  }

  file { 'nginx-host-project':
    path    => "/etc/nginx/sites-available/${host_url}",
    ensure  => file,
    require => Package['nginx'],
    notify  => Service['nginx'],
    content => template('nginx/host.erb'),
  }
  file { 'nginx-host-project-enabled':
    path    => "/etc/nginx/sites-enabled/${host_url}",
    target  => "/etc/nginx/sites-available/${host_url}",
    ensure  => link,
    require => File['nginx-host-project'],
  }

  #file { 'nginx-cert':
  #  path => '/etc/nginx/ssl',
  #  ensure => 'directory',
  #  recurse => 'remote',
  #  source => 'puppet:///modules/nginx/ssl',
  #  require => Package['nginx'],
  #}

  service { 'nginx':
    ensure => running,
    require => Package['nginx'],
  }
}