class mariadb {
  class {'::mysql::server':
    package_name     => 'mariadb-server',
    restart          => true,
    service_name     => 'mysql',
    root_password    => 'root',
    override_options => {
      mysqld => {
        bind-address => '0.0.0.0',
        character-set-server => 'utf8mb4',
        collation-server     => 'utf8mb4_unicode_ci',
      },
    }
  }

  class {'::mysql::client':
    package_name => 'mariadb-client',
  }

  service { 'mariadb':
    ensure  => running,
    require => [Class['::mysql::server'], Class['::mysql::client']]
  }
  
  mysql::db { '*':
    user     => 'root',
    password => 'root',
    host     => '%',
    grant    => ['ALL'],
  }

  mysql::db { "${project}":
    user     => "${project}",
    password => "${project}",
  }

}
